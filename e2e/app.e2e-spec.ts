import { BetaAppPage } from './app.po';

describe('beta-app App', () => {
  let page: BetaAppPage;

  beforeEach(() => {
    page = new BetaAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
